README
======

Provides a collection of useful authentication related tools used for
pulling credentials from Bitwarden and other related services.

Currently the utilities are based on the Bitwarden and AWS CLIs and
these must be installed.

Setup
-----

Install bitwarden from snap:

$ snap install bitwarden

Install ykman CLI:

$ sudo apt-get install yubikey-manager

SSH keys
--------

Entries that begin ssh:: can be used to store SSH private keys, the
key will be downloaded and stored into the SSH agent for a time limited
period.

For more details:

$ bw-ssh-key --help

AWS keys
--------

Entries that begin aws:: can be used to store AWS API keys. These will
be fetched and stored in the current shell session as environment variables.

For more details:

$ bw-aws-key --help
