#!/usr/bin/env python3

import argparse
import boto3
import configparser
import os
import re
import subprocess
import sys

from . import (
    default_args,
    bw_sync,
    bw_get_item,
    bw_item_get_field,
)


def main(argv=None):
    """Fetch SSH keys from bitwarden."""

    argp = argparse.ArgumentParser()

    argp.add_argument(
        'name',
        help='The AWS key to fetch'
    )
    argp.add_argument(
        '--output', action='store_true',
        help='Output the session token to be used in a AWS credential_process config'
    )
    argp.add_argument(
        '-y', '--yubikey', action='store_true',
        help='User yubikey OATH to generate the MFA code'
    )

    default_args(argp)

    args = argp.parse_args(argv)

    if args.sync:
        bw_sync()

    # Fetch the basic access details

    item = bw_get_item(f'aws::{args.name}')

    access_key_id = bw_item_get_field(item, 'aws_access_key_id')['value']
    secret_access_key = bw_item_get_field(item, 'aws_secret_access_key')['value']
    mfa_serial = bw_item_get_field(item, 'mfa_serial')
    mfa_serial = mfa_serial['value'] if mfa_serial else None

    os.environ['AWS_ACCESS_KEY_ID'] = access_key_id
    os.environ['AWS_SECRET_ACCESS_KEY'] = secret_access_key

    # Create a session token
    kwargs = {
        'DurationSeconds': 8 * 3600,  # 8h
    }

    if mfa_serial:

        token = None
        if args.yubikey:
            mfa = f'ykman oath code -s aws_{args.name}'
            token = subprocess.check_output(mfa, shell=True, encoding='utf-8')
            if r := re.search(f'^(aws_{args.name}\\s+)?(\\d+)', token):
                token = r.group(2)
        else:
            sys.stdout.write('Enter MFA token: ')
            sys.stdout.flush()
            token = sys.stdin.readline().strip()

        kwargs['SerialNumber'] = mfa_serial
        kwargs['TokenCode'] = token

    client = boto3.session.Session(
        aws_access_key_id=access_key_id,
        aws_secret_access_key=secret_access_key,
    ).client('sts')
    output = client.get_session_token(**kwargs)

    # Output raw
    if args.output:
        print(output)

    # Add to config
    else:
        sess = output['Credentials']
        conf = configparser.ConfigParser()
        path = os.path.expanduser('~/.aws/credentials')
        if os.path.exists(path):
            conf.read(path)
        conf[args.name] = {
            'aws_access_key_id': sess['AccessKeyId'],
            'aws_secret_access_key': sess['SecretAccessKey'],
            'aws_session_token': sess['SessionToken'],
        }
        conf.write(open(path, 'w'))


if __name__ == '__main__':
    main()
