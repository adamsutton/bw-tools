#!/usr/bin/env python

"""Wrappers around bitwarden cli."""

import json
import subprocess

from importlib.metadata import version


def default_args(argp):
    """Add default arguments."""

    argp.add_argument(
        '-s', '--sync', action='store_true', default=False,
        help='Synchronise the bitwarden vault'
    )
    argp.add_argument(
        '-v', '--version', action='version',
        version='%(prog)s (' + version(__name__.split('.')[0]) + ')'
    )


def bw(cmd):
    """Execute a bitwarden CLI command and read the output."""
    p = subprocess.Popen(f'bw {cmd}', shell=True,
                         stdout=subprocess.PIPE, encoding='utf-8')
    r = p.wait()
    assert r == 0, cmd
    return p.stdout.read()


def bw_sync():
    """Synchronise the vault."""
    bw('sync')


def bw_get_item(name):
    """Fetch a specifc item from the vault."""
    return json.loads(bw(f'get item {name}'))


def bw_item_get_fields(item, name):
    """Extract all values for a field form the item."""
    return [f for f in item['fields'] if f['name'] == name]


def bw_item_get_field(item, name):
    """Extract the first matching field value from the item."""
    items = bw_item_get_fields(item, name)
    if items:
        return items[0]
    return None
