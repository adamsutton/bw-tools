#!/usr/bin/env python3

import argparse
import os
import subprocess
import sys
import tempfile

from . import (
    default_args,
    bw_sync,
    bw_get_item,
    bw_item_get_field,
)


def main(argv=None):
    """Fetch SSH keys from bitwarden."""

    argp = argparse.ArgumentParser()

    argp.add_argument(
        'name', nargs='?',
        help='The SSH key name to fetch'
    )
    argp.add_argument(
        '-p', '--passphrase', action='store_true', default=False,
        help='Fetch the associated passphrase'
    )
    argp.add_argument(
        '-t', '--timeout', type=str, default='8h',
        help='How long the key should be stored in the SSH agent'
    )
    argp.add_argument(
        '-f', '--file', type=str, default=None,
        help='Store the private key in the specific file rather than the SSH agent'
    )

    default_args(argp)

    args = argp.parse_args(argv)

    if args.sync:
        bw_sync()

    name = os.environ['BW_SSH_NAME'] if 'BW_SSH_NAME' in os.environ else args.name

    d = bw_get_item(f'ssh:{name}')

    prvkey = d['notes']+'\n'
    phrase = bw_item_get_field(d, 'Passphrase')['value']

    if args.passphrase or ('BW_SSH_ASKPASS' in os.environ):
        print(phrase)

    elif args.file:
        with open(args.file, 'w') as f:
            os.chmod(args.file, 0o600)
            f.write(prvkey)

    else:
        with tempfile.TemporaryDirectory() as tmpdir:
            os.chdir(tmpdir)

            tmpkey = name

            # Note: using a temporary file just to get useful name in ssh-agent
            with open(tmpkey, 'w') as fp:
                os.chmod(tmpkey, 0o600)
                fp.write(prvkey)

            os.environ['BW_SSH_NAME'] = name
            os.environ['BW_SSH_ASKPASS'] = 'yes'
            os.environ['SSH_ASKPASS'] = sys.argv[0]
            p = subprocess.Popen(f'ssh-add -t {args.timeout} {tmpkey}',
                                 stdin=subprocess.PIPE,
                                 encoding='utf-8',
                                 shell=True, env=os.environ)
            r = p.wait()

            os.unlink(tmpkey)

            assert r == 0


if __name__ == '__main__':
    main()
