#!/usr/bin/env python3

"""Bitwarden Tools."""

import datetime
import os
import re
import setuptools
import subprocess


def version ():
    """Automated version number based on git tags."""
    c = 'git describe --tags --dirty --long --always --abbrev=8 --match "v*"'
    v = subprocess.check_output(c, shell=True, encoding='utf-8')
    r = re.search('v(\d+)\.(\d+)(-(\d+))?-g([0-9a-f]+)', v)
    v = '{}.{}.{}+g{}'.format(r.group(1), r.group(2), r.group(4) or '0', r.group(5))
    return v


def requirements ():
    """Load requirements from file."""
    return [ x.strip() for x in open('requirements.txt').readlines() ]


def find_packages ():
    """Discover all packages."""
    pdir  = { '' : 'src' }
    pkgs  = setuptools.find_packages('src')
    pdata = {}
    for p in pkgs:
        pdata[p] = [ 'data/*' ]
    return (pdir, pkgs, pdata)


pdir, pkgs, pdata = find_packages()

setuptools.setup(
    name                = 'bw_tools',
    version             = version(),
    author              = 'Adam Sutton',
    author_email        = 'dev@adamsutton.me.uk',
    description         = 'Bitwarden Tools',
    license             = 'GPLv3+',
    classifiers         = [
        "Development Status :: 3 - Alpha",
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.10',
        'Topic :: Software Development :: Libraries :: Python Modules',
        "Topic :: Utilities",
    ],
    install_requires    = requirements(),
    packages            = pkgs,
    package_dir         = pdir,
    package_data        = pdata,
    data_files          = [
    ],
    entry_points={
        'console_scripts' : [
            'bw-ssh-key     = bw_tools.ssh:main',
            'bw-aws-key     = bw_tools.aws:main',
        ]
    }
)
