#
# (C) COPYRIGHT Cambridge Consultants Ltd 2017
#
# DESCRIPTION
#     Build the osal-tools package
#
# AUTHOR(S)
#     Adam Sutton
#

ROOT_DIR        := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))
SCRIPTS_DIR     ?= $(ROOT_DIR)/scripts
BUILD_DIR       ?= $(ROOT_DIR)/build
DIST_DIR		?= $(ROOT_DIR)/dist
TOX_DIR         ?= $(ROOT_DIR)/.tox
BUILDENV_RUN    ?= $(SCRIPTS_DIR)/buildenv
IN_DOCKER		?= $(shell cat /proc/1/cgroup | grep -q docker && echo yes)

define docker
	$(if $(IN_DOCKER), $(1), $(BUILDENV_RUN) $(MAKE) -$(MAKEFLAGS) $@)
endef

.PHONY: build test unittest coverage lint publish clean

default: lint build

devel:
	python setup.py develop --user

build wheel:
	$(call docker, \
		python -m tox -e wheel \
	)

test unittest:
	$(call docker, \
		python -m tox -e unittest \
	)

coverage:
	$(call docker, \
		python -m tox -e coverage \
	)

lint:
	$(call docker, \
		python -m tox -e lint \
	)

publish:
	$(call docker, \
		python -m tox -e publish \
	)

clean:
	@rm -rf $(BUILD_DIR) $(DIST_DIR) $(TOX_DIR)
